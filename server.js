const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
//carga los valores con la configuración por defecto
require('dotenv').config();

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}

//pasa a json todo lo que llega en el body
app.use(express.json());
app.use(enableCORS);

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');
const movementController = require('./controllers/MovementController2');

app.listen(port);

console.log("API escuchando en el puerto AFE-SGA" + port);

app.post('/proyecto2019/login', authController.loginUser);
app.post('/proyecto2019/logout', authController.logoutUser);
app.get('/proyecto2019/users/:id', userController.getUserById);
app.post('/proyecto2019/users', userController.createUser);
app.get('/proyecto2019/account/:idUsuario', accountController.getAccountById);
app.post('/proyecto2019/movement', movementController.getMovementsByIBAN);
app.post('/proyecto2019/altaMovement', movementController.altaMovement);
app.post('/proyecto2019/altaContrato', accountController.altaContrato);
