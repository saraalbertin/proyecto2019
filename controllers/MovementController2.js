const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/proyecto2019/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt');
const fecha = require('../fecha');
const accountController = require('./AccountController');

function getMovementsByIBAN(req, res) {
  console.log("Consulta movimientos por IBAN");

  var query = 'q={"IBAN":' + '"' + req.body.IBAN + '"' + '}';
  console.log("La consulta es " + query);

// crea cliente http
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("movement?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {"msg" : "Error obteniendo movimientos de la cuenta"}
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {"msg" : "Cuenta sin movimientos"}
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

function altaMovement(req, res) {
  console.log("Alta movimiento");

  if (req.body.concepto == "Ingreso"){
    var importe = req.body.impOper * (1);
  } else {
    var importe = req.body.impOper * (-1);
  }

  var newMovement = {
    "IBAN": req.body.IBAN,
    "impOper": importe,
    "fecOper": fecha.obtenerFecha(),
    "concepto": req.body.concepto
  }

  insertMovement(newMovement, res);
  accountController.updateAccount(req.body.IBAN, importe);
}

function insertMovement(newMovement, res){
  console.log("insertMovement");

  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.post("movement?" + mLabAPIKey, newMovement,
    function(err, resMLab, body) {
      res.status(201).send({"msg":"Movimiento dado de alta"});
    }
  )
}

module.exports.getMovementsByIBAN = getMovementsByIBAN;
module.exports.altaMovement = altaMovement;
