const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/proyecto2019/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt');
const accountController = require('./AccountController');

function getUserById(req, res) {
  console.log("Consulta usuario para verificar conexión");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {"msg" : "Error obteniendo usuario"}
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
        } else {
          var response = {"msg" : "Usuario no encontrado"}
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

function createUser(req, res) {
  console.log("Alta usuario");
  var query = 's={"id": -1}&l=1';
  console.log("La consulta es: " + query);
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {"msg" : "Error acceso a la tabla de usuarios"}
        res.status(500);
        res.send(response);
      } else {
        if (body.length > 0) {
          var nuevoId = body[0].id + 1;
        } else {
          var nuevoId = 1;
        }

        var newUser = {
          "id": nuevoId,
          "first_name": req.body.first_name,
          "last_name": req.body.last_name,
          "email": req.body.email,
          "password": crypt.hash(req.body.password),
          "logged": true
        }

        var httpClient = requestJson.createClient(baseMLabURL);

        httpClient.post("user?" + mLabAPIKey, newUser,
          function(err, resMLab, body) {
            res.send(newUser);
          }
        )
      }
    }
  )
}

// se pone para poder utilizar la función desde fuera
module.exports.getUserById = getUserById;
module.exports.createUser = createUser;
