const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/proyecto2019/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
//const crypt = require('../crypt');
const fecha = require('../fecha');

function getAccountById(req, res) {
  console.log("Consulta cuenta por ID");

  var idUsuario = req.params.idUsuario;
  var query = 'q={"idUsuario":' + idUsuario + '}';
  console.log("La consulta es" + query);

// crea cliente http
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {"msg" : "Error obteniendo cuenta"}
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {"msg" : "Usuario sin cuentas"}
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

function altaContrato(req, res){
  var nuevoId = req.body.idUsuario;

  var query = 'q={"id":' + nuevoId + '}';
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {"msg" : "Error obteniendo usuario"}
        res.status(500);
      } else {
        if (body.length > 0) {
          if (body.logged == false) {
            var response = "Usuario no conectado";
            res.status(401);
          } else {
            var num1 = Math.random() * 100 + 1;
            var num2 = Math.random() * 10000;
            var num3 = Math.random() * 10000;
            var num4 = Math.random() * 10000;
            var num5 = Math.random() * 10000;
            var num6 = Math.random() * 10000;

            var iban = "ES" + num1.toFixed(0) + " " + num2.toFixed(0) +
                        " " + num3.toFixed(0) + " " + num4.toFixed(0) +
                        " " + num5.toFixed(0) + " " + num6.toFixed(0);

              var newAccount = {
              "idUsuario": nuevoId,
              "IBAN": iban,
              "balance": 0.00,
              "fecApertura": fecha.obtenerFecha()
            }

            var httpClient = requestJson.createClient(baseMLabURL);
            console.log("Client created");

            httpClient.post("account?" + mLabAPIKey, newAccount,
              function(err, resMLab, body) {
                console.log("Cuenta creada en MLAb**");
                var response = {"msg" : "Alta de contrato realizada"}
                res.status(201);
              }
            )
          }
        } else {
          var response = {"msg" : "Usuario no encontrado"}
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

function updateAccount(IBAN, impOper){
  console.log("Actualización de saldo de cuenta");

  var query = 'q={"IBAN":' + '"' + IBAN + '"' + '}';
  console.log("La consulta es " + query);

  // crea cliente http
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, bodyGet) {
      if (bodyGet.length > 0) {
        var newSaldo = parseFloat(impOper) + parseFloat(bodyGet[0].balance);
        var putBody = '{"$set":{"balance":' + newSaldo + '}}';
        httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody));
      } else {
        console.log("Cuenta no encontrada");
      }
    }
  )
}

// se pone para poder utilizar la función desde fuera
module.exports.getAccountById = getAccountById;
module.exports.altaContrato = altaContrato;
module.exports.updateAccount = updateAccount;
