const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/proyecto2019/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt');

function loginUser(req, res) {
  console.log("Login usuario: " + req.body.email);

  var query = 'q={"email": "' + req.body.email + '"}';
  console.log("La consulta es " + query);

  // crea cliente http
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, bodyGet) {
      if (bodyGet.length > 0) {
        var response = bodyGet[0];
        var encontrado = crypt.checkPassword(req.body.password, bodyGet[0].password);
        console.log("Encontrado: " + encontrado);
        if (encontrado) {
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey,
            JSON.parse(putBody))
          var response = {"msg" : "login correcto" , "idUsuario" : bodyGet[0].id}
          res.status(200);
        } else {
          var response = {"msg" : "Password incorrecta"}
          res.status(401);
        }
      } else {
        var response = {"msg" : "Usuario no encontrado"}
        res.status(404);
      }
      res.send(response);
    }
  )

}


function logoutUser(req, res) {
  console.log("Logout usuario: " + req.body.id);

  var query = 'q={"id": ' + req.body.id + '}';
  console.log("La consulta es " + query);

  // crea cliente http
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, bodyGet) {
      if (bodyGet.length > 0) {
        if (bodyGet[0].logged == true) {
          var putBody = '{"$unset":{"logged":""}}'
          httpClient.put("user?" + query + "&" + mLabAPIKey,
             JSON.parse(putBody))
          var response = {"mensaje" : "logout correcto" , "idUsuario" : bodyGet[0].id}
        } else {
          var response = {"msg" : "Usuario no conectado"}
        }
      } else {
        var response = {"msg" : "Usuario no encontrado"}
      }
      res.send(response);
    }
  )
}

module.exports.loginUser = loginUser;
module.exports.logoutUser = logoutUser;
