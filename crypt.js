const bcrypt = require('bcrypt');

//devuelve un string encriptado. Encripta data
function hash(data) {
  return bcrypt.hashSync(data, 10);
}

function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
  return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
